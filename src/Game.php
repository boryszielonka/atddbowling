<?php

namespace Bowling;

Class Game
{

    /** @var int */

    private $rounds;

    /** @var int */
    private $score;

    /** @var int */
    private $currentRound;

    /** @var bool */
    private $double;

    function __construct(int $rounds)
    {
        $this->rounds = $rounds;
    }

    public function bowl(string ...$scores)
    {


        if ($this->currentRound >= $this->rounds) {
            throw new \Exception();
        }


        if ($scores[1] === '/') {
            $this->score += 10;
            $this->double = true;
        } elseif ($this->double) {
            $this->double = false;
            $this->score += 2 * $scores[0];
            $this->score += $scores[1];
        } else {
            $this->double = false;

            $this->score += array_sum($scores);
        }

        $this->currentRound++;

    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }
}